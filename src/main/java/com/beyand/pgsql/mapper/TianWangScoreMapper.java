package com.beyand.pgsql.mapper;

import com.beyand.pgsql.domain.MultiBorrowTag;
import com.beyand.pgsql.domain.SkynetSaturabilityRaw;

/**
 * 账户 数据层
 * 
 * @author bian
 */
public interface TianWangScoreMapper
{
    //获取天网表bean
    SkynetSaturabilityRaw getSkeyNetBeanById(String idNumber);


    //获取共贷表bean
    MultiBorrowTag getMutiBrowBeanById(String idNumber);
}
