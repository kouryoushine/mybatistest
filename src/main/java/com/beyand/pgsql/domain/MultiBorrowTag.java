package com.beyand.pgsql.domain;

public class MultiBorrowTag {
    /**  */
    private String idhashsha256;
    /**  */
    private int  queryCnt1m;
    /**  */
    private int  queryOrgcnt1m;
    /**  */
    private int  queryCnt3m;
    /**  */
    private int  queryOrgcnt3m;
    /**  */
    private int  queryCntTotal;
    /**  */
    private int  queryOrgcntTotal;
    /**  */
    private int  queryType1Cnt1m;
    /**  */
    private int  queryType1Orgcnt1m;
    /**  */
    private int  queryType2Cnt1m;
    /**  */
    private int  queryType2Orgcnt1m;
    /**  */
    private int  queryType3Cnt1m;
    /**  */
    private int  queryType3Orgcnt1m;
    /**  */
    private int  queryType1Cnt3m;
    /**  */
    private int  queryType1Orgcnt3m;
    /**  */
    private int  queryType2Cnt3m;
    /**  */
    private int  queryType2Orgcnt3m;
    /**  */
    private int  queryType3Cnt3m;
    /**  */
    private int  queryType3Orgcnt3m;
    /**  */
    private int  queryType1Cnt;
    /**  */
    private int  queryType1Orgcnt;
    /**  */
    private int  queryType2Cnt;
    /**  */
    private int  queryType2Orgcnt;
    /**  */
    private int  queryType3Cnt;
    /**  */
    private int  queryType3Orgcnt;
    /**  */
    private int  queryConsumfinCnt1m;
    /**  */
    private int  queryConsumfinOrgcnt1m;
    /**  */
    private int  querySmallloanCnt1m;
    /**  */
    private int  querySmallloanOrgcnt1m;
    /**  */
    private int  queryCarloanCnt1m;
    /**  */
    private int  queryCarloanOrgcnt1m;
    /**  */
    private int  queryConsumfinCnt3m;
    /**  */
    private int  queryConsumfinOrgcnt3m;
    /**  */
    private int  querySmallloanCnt3m;
    /**  */
    private int  querySmallloanOrgcnt3m;
    /**  */
    private int  queryCarloanCnt3m;
    /**  */
    private int  queryCarloanOrgcnt3m;
    /**  */
    private int  queryConsumfinCnt;
    /**  */
    private int  queryConsumfinOrgcnt;
    /**  */
    private int  querySmallloanCnt;
    /**  */
    private int  querySmallloanOrgcnt;
    /**  */
    private int  queryCarloanCnt;
    /**  */
    private int  queryCarloanOrgcnt;
    /**  */
    private int  queryIdnumMatchnameCnt;
    /**  */
    private int  queryMaxCntIn1d;
    /**  */
    private int  queryMaxOrgcntIn1d;
    /**  */
    private int  queryNetworkloanMonths;
    /**  */
    private int  queryGapDayMin;
    /**  */
    private int  queryGapDayMax;

    public String getIdhashsha256() {
        return idhashsha256;
    }

    public void setIdhashsha256(String idhashsha256) {
        this.idhashsha256 = idhashsha256;
    }

    public int  getQueryCnt1m() {
        return queryCnt1m;
    }

    public void setQueryCnt1m(int  queryCnt1m) {
        this.queryCnt1m = queryCnt1m;
    }

    public int  getQueryOrgcnt1m() {
        return queryOrgcnt1m;
    }

    public void setQueryOrgcnt1m(int  queryOrgcnt1m) {
        this.queryOrgcnt1m = queryOrgcnt1m;
    }

    public int  getQueryCnt3m() {
        return queryCnt3m;
    }

    public void setQueryCnt3m(int  queryCnt3m) {
        this.queryCnt3m = queryCnt3m;
    }

    public int  getQueryOrgcnt3m() {
        return queryOrgcnt3m;
    }

    public void setQueryOrgcnt3m(int  queryOrgcnt3m) {
        this.queryOrgcnt3m = queryOrgcnt3m;
    }

    public int  getQueryCntTotal() {
        return queryCntTotal;
    }

    public void setQueryCntTotal(int  queryCntTotal) {
        this.queryCntTotal = queryCntTotal;
    }

    public int  getQueryOrgcntTotal() {
        return queryOrgcntTotal;
    }

    public void setQueryOrgcntTotal(int  queryOrgcntTotal) {
        this.queryOrgcntTotal = queryOrgcntTotal;
    }

    public int  getQueryType1Cnt1m() {
        return queryType1Cnt1m;
    }

    public void setQueryType1Cnt1m(int  queryType1Cnt1m) {
        this.queryType1Cnt1m = queryType1Cnt1m;
    }

    public int  getQueryType1Orgcnt1m() {
        return queryType1Orgcnt1m;
    }

    public void setQueryType1Orgcnt1m(int  queryType1Orgcnt1m) {
        this.queryType1Orgcnt1m = queryType1Orgcnt1m;
    }

    public int  getQueryType2Cnt1m() {
        return queryType2Cnt1m;
    }

    public void setQueryType2Cnt1m(int  queryType2Cnt1m) {
        this.queryType2Cnt1m = queryType2Cnt1m;
    }

    public int  getQueryType2Orgcnt1m() {
        return queryType2Orgcnt1m;
    }

    public void setQueryType2Orgcnt1m(int  queryType2Orgcnt1m) {
        this.queryType2Orgcnt1m = queryType2Orgcnt1m;
    }

    public int  getQueryType3Cnt1m() {
        return queryType3Cnt1m;
    }

    public void setQueryType3Cnt1m(int  queryType3Cnt1m) {
        this.queryType3Cnt1m = queryType3Cnt1m;
    }

    public int  getQueryType3Orgcnt1m() {
        return queryType3Orgcnt1m;
    }

    public void setQueryType3Orgcnt1m(int  queryType3Orgcnt1m) {
        this.queryType3Orgcnt1m = queryType3Orgcnt1m;
    }

    public int  getQueryType1Cnt3m() {
        return queryType1Cnt3m;
    }

    public void setQueryType1Cnt3m(int  queryType1Cnt3m) {
        this.queryType1Cnt3m = queryType1Cnt3m;
    }

    public int  getQueryType1Orgcnt3m() {
        return queryType1Orgcnt3m;
    }

    public void setQueryType1Orgcnt3m(int  queryType1Orgcnt3m) {
        this.queryType1Orgcnt3m = queryType1Orgcnt3m;
    }

    public int  getQueryType2Cnt3m() {
        return queryType2Cnt3m;
    }

    public void setQueryType2Cnt3m(int  queryType2Cnt3m) {
        this.queryType2Cnt3m = queryType2Cnt3m;
    }

    public int  getQueryType2Orgcnt3m() {
        return queryType2Orgcnt3m;
    }

    public void setQueryType2Orgcnt3m(int  queryType2Orgcnt3m) {
        this.queryType2Orgcnt3m = queryType2Orgcnt3m;
    }

    public int  getQueryType3Cnt3m() {
        return queryType3Cnt3m;
    }

    public void setQueryType3Cnt3m(int  queryType3Cnt3m) {
        this.queryType3Cnt3m = queryType3Cnt3m;
    }

    public int  getQueryType3Orgcnt3m() {
        return queryType3Orgcnt3m;
    }

    public void setQueryType3Orgcnt3m(int  queryType3Orgcnt3m) {
        this.queryType3Orgcnt3m = queryType3Orgcnt3m;
    }

    public int  getQueryType1Cnt() {
        return queryType1Cnt;
    }

    public void setQueryType1Cnt(int  queryType1Cnt) {
        this.queryType1Cnt = queryType1Cnt;
    }

    public int  getQueryType1Orgcnt() {
        return queryType1Orgcnt;
    }

    public void setQueryType1Orgcnt(int  queryType1Orgcnt) {
        this.queryType1Orgcnt = queryType1Orgcnt;
    }

    public int  getQueryType2Cnt() {
        return queryType2Cnt;
    }

    public void setQueryType2Cnt(int  queryType2Cnt) {
        this.queryType2Cnt = queryType2Cnt;
    }

    public int  getQueryType2Orgcnt() {
        return queryType2Orgcnt;
    }

    public void setQueryType2Orgcnt(int  queryType2Orgcnt) {
        this.queryType2Orgcnt = queryType2Orgcnt;
    }

    public int  getQueryType3Cnt() {
        return queryType3Cnt;
    }

    public void setQueryType3Cnt(int  queryType3Cnt) {
        this.queryType3Cnt = queryType3Cnt;
    }

    public int  getQueryType3Orgcnt() {
        return queryType3Orgcnt;
    }

    public void setQueryType3Orgcnt(int  queryType3Orgcnt) {
        this.queryType3Orgcnt = queryType3Orgcnt;
    }

    public int  getQueryConsumfinCnt1m() {
        return queryConsumfinCnt1m;
    }

    public void setQueryConsumfinCnt1m(int  queryConsumfinCnt1m) {
        this.queryConsumfinCnt1m = queryConsumfinCnt1m;
    }

    public int  getQueryConsumfinOrgcnt1m() {
        return queryConsumfinOrgcnt1m;
    }

    public void setQueryConsumfinOrgcnt1m(int  queryConsumfinOrgcnt1m) {
        this.queryConsumfinOrgcnt1m = queryConsumfinOrgcnt1m;
    }

    public int  getQuerySmallloanCnt1m() {
        return querySmallloanCnt1m;
    }

    public void setQuerySmallloanCnt1m(int  querySmallloanCnt1m) {
        this.querySmallloanCnt1m = querySmallloanCnt1m;
    }

    public int  getQuerySmallloanOrgcnt1m() {
        return querySmallloanOrgcnt1m;
    }

    public void setQuerySmallloanOrgcnt1m(int  querySmallloanOrgcnt1m) {
        this.querySmallloanOrgcnt1m = querySmallloanOrgcnt1m;
    }

    public int  getQueryCarloanCnt1m() {
        return queryCarloanCnt1m;
    }

    public void setQueryCarloanCnt1m(int  queryCarloanCnt1m) {
        this.queryCarloanCnt1m = queryCarloanCnt1m;
    }

    public int  getQueryCarloanOrgcnt1m() {
        return queryCarloanOrgcnt1m;
    }

    public void setQueryCarloanOrgcnt1m(int  queryCarloanOrgcnt1m) {
        this.queryCarloanOrgcnt1m = queryCarloanOrgcnt1m;
    }

    public int  getQueryConsumfinCnt3m() {
        return queryConsumfinCnt3m;
    }

    public void setQueryConsumfinCnt3m(int  queryConsumfinCnt3m) {
        this.queryConsumfinCnt3m = queryConsumfinCnt3m;
    }

    public int  getQueryConsumfinOrgcnt3m() {
        return queryConsumfinOrgcnt3m;
    }

    public void setQueryConsumfinOrgcnt3m(int  queryConsumfinOrgcnt3m) {
        this.queryConsumfinOrgcnt3m = queryConsumfinOrgcnt3m;
    }

    public int  getQuerySmallloanCnt3m() {
        return querySmallloanCnt3m;
    }

    public void setQuerySmallloanCnt3m(int  querySmallloanCnt3m) {
        this.querySmallloanCnt3m = querySmallloanCnt3m;
    }

    public int  getQuerySmallloanOrgcnt3m() {
        return querySmallloanOrgcnt3m;
    }

    public void setQuerySmallloanOrgcnt3m(int  querySmallloanOrgcnt3m) {
        this.querySmallloanOrgcnt3m = querySmallloanOrgcnt3m;
    }

    public int  getQueryCarloanCnt3m() {
        return queryCarloanCnt3m;
    }

    public void setQueryCarloanCnt3m(int  queryCarloanCnt3m) {
        this.queryCarloanCnt3m = queryCarloanCnt3m;
    }

    public int  getQueryCarloanOrgcnt3m() {
        return queryCarloanOrgcnt3m;
    }

    public void setQueryCarloanOrgcnt3m(int  queryCarloanOrgcnt3m) {
        this.queryCarloanOrgcnt3m = queryCarloanOrgcnt3m;
    }

    public int  getQueryConsumfinCnt() {
        return queryConsumfinCnt;
    }

    public void setQueryConsumfinCnt(int  queryConsumfinCnt) {
        this.queryConsumfinCnt = queryConsumfinCnt;
    }

    public int  getQueryConsumfinOrgcnt() {
        return queryConsumfinOrgcnt;
    }

    public void setQueryConsumfinOrgcnt(int  queryConsumfinOrgcnt) {
        this.queryConsumfinOrgcnt = queryConsumfinOrgcnt;
    }

    public int  getQuerySmallloanCnt() {
        return querySmallloanCnt;
    }

    public void setQuerySmallloanCnt(int  querySmallloanCnt) {
        this.querySmallloanCnt = querySmallloanCnt;
    }

    public int  getQuerySmallloanOrgcnt() {
        return querySmallloanOrgcnt;
    }

    public void setQuerySmallloanOrgcnt(int  querySmallloanOrgcnt) {
        this.querySmallloanOrgcnt = querySmallloanOrgcnt;
    }

    public int  getQueryCarloanCnt() {
        return queryCarloanCnt;
    }

    public void setQueryCarloanCnt(int  queryCarloanCnt) {
        this.queryCarloanCnt = queryCarloanCnt;
    }

    public int  getQueryCarloanOrgcnt() {
        return queryCarloanOrgcnt;
    }

    public void setQueryCarloanOrgcnt(int  queryCarloanOrgcnt) {
        this.queryCarloanOrgcnt = queryCarloanOrgcnt;
    }

    public int  getQueryIdnumMatchnameCnt() {
        return queryIdnumMatchnameCnt;
    }

    public void setQueryIdnumMatchnameCnt(int  queryIdnumMatchnameCnt) {
        this.queryIdnumMatchnameCnt = queryIdnumMatchnameCnt;
    }

    public int  getQueryMaxCntIn1d() {
        return queryMaxCntIn1d;
    }

    public void setQueryMaxCntIn1d(int  queryMaxCntIn1d) {
        this.queryMaxCntIn1d = queryMaxCntIn1d;
    }

    public int  getQueryMaxOrgcntIn1d() {
        return queryMaxOrgcntIn1d;
    }

    public void setQueryMaxOrgcntIn1d(int  queryMaxOrgcntIn1d) {
        this.queryMaxOrgcntIn1d = queryMaxOrgcntIn1d;
    }

    public int  getQueryNetworkloanMonths() {
        return queryNetworkloanMonths;
    }

    public void setQueryNetworkloanMonths(int  queryNetworkloanMonths) {
        this.queryNetworkloanMonths = queryNetworkloanMonths;
    }

    public int  getQueryGapDayMin() {
        return queryGapDayMin;
    }

    public void setQueryGapDayMin(int  queryGapDayMin) {
        this.queryGapDayMin = queryGapDayMin;
    }

    public int  getQueryGapDayMax() {
        return queryGapDayMax;
    }

    public void setQueryGapDayMax(int  queryGapDayMax) {
        this.queryGapDayMax = queryGapDayMax;
    }

    public String toData() {
        final StringBuilder sb = new StringBuilder("{");
        sb.append("\"queryCnt1m\":")
                .append(queryCnt1m);
        sb.append(",\"queryOrgcnt1m\":")
                .append(queryOrgcnt1m);
        sb.append(",\"queryCnt3m\":")
                .append(queryCnt3m);
        sb.append(",\"queryOrgcnt3m\":")
                .append(queryOrgcnt3m);
        sb.append(",\"queryCntTotal\":")
                .append(queryCntTotal);
        sb.append(",\"queryOrgcntTotal\":")
                .append(queryOrgcntTotal);
        sb.append(",\"queryType1Cnt1m\":")
                .append(queryType1Cnt1m);
        sb.append(",\"queryType1Orgcnt1m\":")
                .append(queryType1Orgcnt1m);
        sb.append(",\"queryType2Cnt1m\":")
                .append(queryType2Cnt1m);
        sb.append(",\"queryType2Orgcnt1m\":")
                .append(queryType2Orgcnt1m);
        sb.append(",\"queryType3Cnt1m\":")
                .append(queryType3Cnt1m);
        sb.append(",\"queryType3Orgcnt1m\":")
                .append(queryType3Orgcnt1m);
        sb.append(",\"queryType1Cnt3m\":")
                .append(queryType1Cnt3m);
        sb.append(",\"queryType1Orgcnt3m\":")
                .append(queryType1Orgcnt3m);
        sb.append(",\"queryType2Cnt3m\":")
                .append(queryType2Cnt3m);
        sb.append(",\"queryType2Orgcnt3m\":")
                .append(queryType2Orgcnt3m);
        sb.append(",\"queryType3Cnt3m\":")
                .append(queryType3Cnt3m);
        sb.append(",\"queryType3Orgcnt3m\":")
                .append(queryType3Orgcnt3m);
        sb.append(",\"queryType1Cnt\":")
                .append(queryType1Cnt);
        sb.append(",\"queryType1Orgcnt\":")
                .append(queryType1Orgcnt);
        sb.append(",\"queryType2Cnt\":")
                .append(queryType2Cnt);
        sb.append(",\"queryType2Orgcnt\":")
                .append(queryType2Orgcnt);
        sb.append(",\"queryType3Cnt\":")
                .append(queryType3Cnt);
        sb.append(",\"queryType3Orgcnt\":")
                .append(queryType3Orgcnt);
        sb.append(",\"queryConsumfinCnt1m\":")
                .append(queryConsumfinCnt1m);
        sb.append(",\"queryConsumfinOrgcnt1m\":")
                .append(queryConsumfinOrgcnt1m);
        sb.append(",\"querySmallloanCnt1m\":")
                .append(querySmallloanCnt1m);
        sb.append(",\"querySmallloanOrgcnt1m\":")
                .append(querySmallloanOrgcnt1m);
        sb.append(",\"queryCarloanCnt1m\":")
                .append(queryCarloanCnt1m);
        sb.append(",\"queryCarloanOrgcnt1m\":")
                .append(queryCarloanOrgcnt1m);
        sb.append(",\"queryConsumfinCnt3m\":")
                .append(queryConsumfinCnt3m);
        sb.append(",\"queryConsumfinOrgcnt3m\":")
                .append(queryConsumfinOrgcnt3m);
        sb.append(",\"querySmallloanCnt3m\":")
                .append(querySmallloanCnt3m);
        sb.append(",\"querySmallloanOrgcnt3m\":")
                .append(querySmallloanOrgcnt3m);
        sb.append(",\"queryCarloanCnt3m\":")
                .append(queryCarloanCnt3m);
        sb.append(",\"queryCarloanOrgcnt3m\":")
                .append(queryCarloanOrgcnt3m);
        sb.append(",\"queryConsumfinCnt\":")
                .append(queryConsumfinCnt);
        sb.append(",\"queryConsumfinOrgcnt\":")
                .append(queryConsumfinOrgcnt);
        sb.append(",\"querySmallloanCnt\":")
                .append(querySmallloanCnt);
        sb.append(",\"querySmallloanOrgcnt\":")
                .append(querySmallloanOrgcnt);
        sb.append(",\"queryCarloanCnt\":")
                .append(queryCarloanCnt);
        sb.append(",\"queryCarloanOrgcnt\":")
                .append(queryCarloanOrgcnt);
        sb.append(",\"queryIdnumMatchnameCnt\":")
                .append(queryIdnumMatchnameCnt);
        sb.append(",\"queryMaxCntIn1d\":")
                .append(queryMaxCntIn1d);
        sb.append(",\"queryMaxOrgcntIn1d\":")
                .append(queryMaxOrgcntIn1d);
        sb.append(",\"queryNetworkloanMonths\":")
                .append(queryNetworkloanMonths);
        sb.append(",\"queryGapDayMin\":")
                .append(queryGapDayMin);
        sb.append(",\"queryGapDayMax\":")
                .append(queryGapDayMax);
        sb.append('}');
        return sb.toString();
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("{");
        sb.append("\"idhashsha256\":\"")
                .append(idhashsha256).append('\"');
        sb.append(",\"queryCnt1m\":")
                .append(queryCnt1m);
        sb.append(",\"queryOrgcnt1m\":")
                .append(queryOrgcnt1m);
        sb.append(",\"queryCnt3m\":")
                .append(queryCnt3m);
        sb.append(",\"queryOrgcnt3m\":")
                .append(queryOrgcnt3m);
        sb.append(",\"queryCntTotal\":")
                .append(queryCntTotal);
        sb.append(",\"queryOrgcntTotal\":")
                .append(queryOrgcntTotal);
        sb.append(",\"queryType1Cnt1m\":")
                .append(queryType1Cnt1m);
        sb.append(",\"queryType1Orgcnt1m\":")
                .append(queryType1Orgcnt1m);
        sb.append(",\"queryType2Cnt1m\":")
                .append(queryType2Cnt1m);
        sb.append(",\"queryType2Orgcnt1m\":")
                .append(queryType2Orgcnt1m);
        sb.append(",\"queryType3Cnt1m\":")
                .append(queryType3Cnt1m);
        sb.append(",\"queryType3Orgcnt1m\":")
                .append(queryType3Orgcnt1m);
        sb.append(",\"queryType1Cnt3m\":")
                .append(queryType1Cnt3m);
        sb.append(",\"queryType1Orgcnt3m\":")
                .append(queryType1Orgcnt3m);
        sb.append(",\"queryType2Cnt3m\":")
                .append(queryType2Cnt3m);
        sb.append(",\"queryType2Orgcnt3m\":")
                .append(queryType2Orgcnt3m);
        sb.append(",\"queryType3Cnt3m\":")
                .append(queryType3Cnt3m);
        sb.append(",\"queryType3Orgcnt3m\":")
                .append(queryType3Orgcnt3m);
        sb.append(",\"queryType1Cnt\":")
                .append(queryType1Cnt);
        sb.append(",\"queryType1Orgcnt\":")
                .append(queryType1Orgcnt);
        sb.append(",\"queryType2Cnt\":")
                .append(queryType2Cnt);
        sb.append(",\"queryType2Orgcnt\":")
                .append(queryType2Orgcnt);
        sb.append(",\"queryType3Cnt\":")
                .append(queryType3Cnt);
        sb.append(",\"queryType3Orgcnt\":")
                .append(queryType3Orgcnt);
        sb.append(",\"queryConsumfinCnt1m\":")
                .append(queryConsumfinCnt1m);
        sb.append(",\"queryConsumfinOrgcnt1m\":")
                .append(queryConsumfinOrgcnt1m);
        sb.append(",\"querySmallloanCnt1m\":")
                .append(querySmallloanCnt1m);
        sb.append(",\"querySmallloanOrgcnt1m\":")
                .append(querySmallloanOrgcnt1m);
        sb.append(",\"queryCarloanCnt1m\":")
                .append(queryCarloanCnt1m);
        sb.append(",\"queryCarloanOrgcnt1m\":")
                .append(queryCarloanOrgcnt1m);
        sb.append(",\"queryConsumfinCnt3m\":")
                .append(queryConsumfinCnt3m);
        sb.append(",\"queryConsumfinOrgcnt3m\":")
                .append(queryConsumfinOrgcnt3m);
        sb.append(",\"querySmallloanCnt3m\":")
                .append(querySmallloanCnt3m);
        sb.append(",\"querySmallloanOrgcnt3m\":")
                .append(querySmallloanOrgcnt3m);
        sb.append(",\"queryCarloanCnt3m\":")
                .append(queryCarloanCnt3m);
        sb.append(",\"queryCarloanOrgcnt3m\":")
                .append(queryCarloanOrgcnt3m);
        sb.append(",\"queryConsumfinCnt\":")
                .append(queryConsumfinCnt);
        sb.append(",\"queryConsumfinOrgcnt\":")
                .append(queryConsumfinOrgcnt);
        sb.append(",\"querySmallloanCnt\":")
                .append(querySmallloanCnt);
        sb.append(",\"querySmallloanOrgcnt\":")
                .append(querySmallloanOrgcnt);
        sb.append(",\"queryCarloanCnt\":")
                .append(queryCarloanCnt);
        sb.append(",\"queryCarloanOrgcnt\":")
                .append(queryCarloanOrgcnt);
        sb.append(",\"queryIdnumMatchnameCnt\":")
                .append(queryIdnumMatchnameCnt);
        sb.append(",\"queryMaxCntIn1d\":")
                .append(queryMaxCntIn1d);
        sb.append(",\"queryMaxOrgcntIn1d\":")
                .append(queryMaxOrgcntIn1d);
        sb.append(",\"queryNetworkloanMonths\":")
                .append(queryNetworkloanMonths);
        sb.append(",\"queryGapDayMin\":")
                .append(queryGapDayMin);
        sb.append(",\"queryGapDayMax\":")
                .append(queryGapDayMax);
        sb.append('}');
        return sb.toString();
    }
}
