package com.beyand.pgsql.domain;

public class SkynetSaturabilityRaw {

    /**  */
    private String idhashsha256;
    /**  */
    private int  provinceLine;
    /**  */
    private String province;
    /**  */
    private int  citylevel;
    /**  */
    private int  avgAmountPerMonthLevel;
    /**  */
    private int  judicativeType;
    /**  */
    private int  isnotwork;
    /**  */
    private int  gender;
    /**  */
    private int  networkLoan;
    /**  */
    private int  hascarOrnot;
    /**  */
    private int  collectionLoan;
    /**  */
    private int  losecreditTag;
    /**  */
    private int  age;
    /**  */
    private int  amountPerMonth;
    /**  */
    private int  salary;
    /**  */
    private int  flightcount;
    /**  */
    private int  fundLastCompanyMonthNum;
    /**  */
    private int  companyNumLast24month;
    /**  */
    private int  idnumNameCount;
    /**  */
    private int  idnumMobileCount;
    /**  */
    private int  idnumEmailCount;
    /**  */
    private int  idnumQqCount;
    /**  */
    private int  imeiCount;
    /**  */
    private int  flightTime;
    /**  */
    private int  trainTakerCount;
    /**  */
    private int  trainBuyerCount;
    /**  */
    private int  investInfo;
    /**  */
    private int  eBusinessCount;
    /**  */
    private int  blackListA1;
    /**  */
    private int  blackListA2;
    /**  */
    private int  blackListA3;
    /**  */
    private int  blackListA4;
    /**  */
    private int  blackListA5;
    /**  */
    private int  blackListA6;
    /**  */
    private int  blackListA7;
    /**  */
    private int  blackListA8;
    /**  */
    private int  blackListA9;
    /**  */
    private int  blackListA10;
    /**  */
    private int  blackListA11;
    /**  */
    private int  blackListB20;
    /**  */
    private int  blackListB50;
    /**  */
    private int  blackListB51;
    /**  */
    private int  gdTypeZzrCount;
    /**  */
    private int  gdTypeTzzCount;
    /**  */
    private int  gdTypeQyCount;
    /**  */
    private int  gdTypeQtCount;
    /**  */
    private int  gdTypeFrCount;
    /**  */
    private int  corpCxCount;
    /**  */
    private int  corpTyCount;
    /**  */
    private int  corpDxCount;
    /**  */
    private int  corpZxCount;
    /**  */
    private int  corpQcCount;
    /**  */
    private int  corpQtCount;
    /**  */
    private int  individualCxCount;
    /**  */
    private int  individualTyCount;
    /**  */
    private int  individualDxCount;
    /**  */
    private int  individualZxCount;
    /**  */
    private int  individualQcCount;
    /**  */
    private int  individualQtCount;
    /**  */
    private int  executiveinfoCount;
    /**  */
    private String provinceCode;
    /**  */
    private String cityCode;

    public String getIdhashsha256() {
        return idhashsha256;
    }

    public void setIdhashsha256(String idhashsha256) {
        this.idhashsha256 = idhashsha256;
    }

    public int  getProvinceLine() {
        return provinceLine;
    }

    public void setProvinceLine(int  provinceLine) {
        this.provinceLine = provinceLine;
    }

    public String getProvince() {
        return province;
    }

    public void setProvince(String province) {
        this.province = province;
    }

    public int  getCitylevel() {
        return citylevel;
    }

    public void setCitylevel(int  citylevel) {
        this.citylevel = citylevel;
    }

    public int  getAvgAmountPerMonthLevel() {
        return avgAmountPerMonthLevel;
    }

    public void setAvgAmountPerMonthLevel(int  avgAmountPerMonthLevel) {
        this.avgAmountPerMonthLevel = avgAmountPerMonthLevel;
    }

    public int  getJudicativeType() {
        return judicativeType;
    }

    public void setJudicativeType(int  judicativeType) {
        this.judicativeType = judicativeType;
    }

    public int  getIsnotwork() {
        return isnotwork;
    }

    public void setIsnotwork(int  isnotwork) {
        this.isnotwork = isnotwork;
    }

    public int  getGender() {
        return gender;
    }

    public void setGender(int  gender) {
        this.gender = gender;
    }

    public int  getNetworkLoan() {
        return networkLoan;
    }

    public void setNetworkLoan(int  networkLoan) {
        this.networkLoan = networkLoan;
    }

    public int  getHascarOrnot() {
        return hascarOrnot;
    }

    public void setHascarOrnot(int  hascarOrnot) {
        this.hascarOrnot = hascarOrnot;
    }

    public int  getCollectionLoan() {
        return collectionLoan;
    }

    public void setCollectionLoan(int  collectionLoan) {
        this.collectionLoan = collectionLoan;
    }

    public int  getLosecreditTag() {
        return losecreditTag;
    }

    public void setLosecreditTag(int  losecreditTag) {
        this.losecreditTag = losecreditTag;
    }

    public int  getAge() {
        return age;
    }

    public void setAge(int  age) {
        this.age = age;
    }

    public int  getAmountPerMonth() {
        return amountPerMonth;
    }

    public void setAmountPerMonth(int  amountPerMonth) {
        this.amountPerMonth = amountPerMonth;
    }

    public int  getSalary() {
        return salary;
    }

    public void setSalary(int  salary) {
        this.salary = salary;
    }

    public int  getFlightcount() {
        return flightcount;
    }

    public void setFlightcount(int  flightcount) {
        this.flightcount = flightcount;
    }

    public int  getFundLastCompanyMonthNum() {
        return fundLastCompanyMonthNum;
    }

    public void setFundLastCompanyMonthNum(int  fundLastCompanyMonthNum) {
        this.fundLastCompanyMonthNum = fundLastCompanyMonthNum;
    }

    public int  getCompanyNumLast24month() {
        return companyNumLast24month;
    }

    public void setCompanyNumLast24month(int  companyNumLast24month) {
        this.companyNumLast24month = companyNumLast24month;
    }

    public int  getIdnumNameCount() {
        return idnumNameCount;
    }

    public void setIdnumNameCount(int  idnumNameCount) {
        this.idnumNameCount = idnumNameCount;
    }

    public int  getIdnumMobileCount() {
        return idnumMobileCount;
    }

    public void setIdnumMobileCount(int  idnumMobileCount) {
        this.idnumMobileCount = idnumMobileCount;
    }

    public int  getIdnumEmailCount() {
        return idnumEmailCount;
    }

    public void setIdnumEmailCount(int  idnumEmailCount) {
        this.idnumEmailCount = idnumEmailCount;
    }

    public int  getIdnumQqCount() {
        return idnumQqCount;
    }

    public void setIdnumQqCount(int  idnumQqCount) {
        this.idnumQqCount = idnumQqCount;
    }

    public int  getImeiCount() {
        return imeiCount;
    }

    public void setImeiCount(int  imeiCount) {
        this.imeiCount = imeiCount;
    }

    public int  getFlightTime() {
        return flightTime;
    }

    public void setFlightTime(int  flightTime) {
        this.flightTime = flightTime;
    }

    public int  getTrainTakerCount() {
        return trainTakerCount;
    }

    public void setTrainTakerCount(int  trainTakerCount) {
        this.trainTakerCount = trainTakerCount;
    }

    public int  getTrainBuyerCount() {
        return trainBuyerCount;
    }

    public void setTrainBuyerCount(int  trainBuyerCount) {
        this.trainBuyerCount = trainBuyerCount;
    }

    public int  getInvestInfo() {
        return investInfo;
    }

    public void setInvestInfo(int  investInfo) {
        this.investInfo = investInfo;
    }

    public int  geteBusinessCount() {
        return eBusinessCount;
    }

    public void seteBusinessCount(int  eBusinessCount) {
        this.eBusinessCount = eBusinessCount;
    }

    public int  getBlackListA1() {
        return blackListA1;
    }

    public void setBlackListA1(int  blackListA1) {
        this.blackListA1 = blackListA1;
    }

    public int  getBlackListA2() {
        return blackListA2;
    }

    public void setBlackListA2(int  blackListA2) {
        this.blackListA2 = blackListA2;
    }

    public int  getBlackListA3() {
        return blackListA3;
    }

    public void setBlackListA3(int  blackListA3) {
        this.blackListA3 = blackListA3;
    }

    public int  getBlackListA4() {
        return blackListA4;
    }

    public void setBlackListA4(int  blackListA4) {
        this.blackListA4 = blackListA4;
    }

    public int  getBlackListA5() {
        return blackListA5;
    }

    public void setBlackListA5(int  blackListA5) {
        this.blackListA5 = blackListA5;
    }

    public int  getBlackListA6() {
        return blackListA6;
    }

    public void setBlackListA6(int  blackListA6) {
        this.blackListA6 = blackListA6;
    }

    public int  getBlackListA7() {
        return blackListA7;
    }

    public void setBlackListA7(int  blackListA7) {
        this.blackListA7 = blackListA7;
    }

    public int  getBlackListA8() {
        return blackListA8;
    }

    public void setBlackListA8(int  blackListA8) {
        this.blackListA8 = blackListA8;
    }

    public int  getBlackListA9() {
        return blackListA9;
    }

    public void setBlackListA9(int  blackListA9) {
        this.blackListA9 = blackListA9;
    }

    public int  getBlackListA10() {
        return blackListA10;
    }

    public void setBlackListA10(int  blackListA10) {
        this.blackListA10 = blackListA10;
    }

    public int  getBlackListA11() {
        return blackListA11;
    }

    public void setBlackListA11(int  blackListA11) {
        this.blackListA11 = blackListA11;
    }

    public int  getBlackListB20() {
        return blackListB20;
    }

    public void setBlackListB20(int  blackListB20) {
        this.blackListB20 = blackListB20;
    }

    public int  getBlackListB50() {
        return blackListB50;
    }

    public void setBlackListB50(int  blackListB50) {
        this.blackListB50 = blackListB50;
    }

    public int  getBlackListB51() {
        return blackListB51;
    }

    public void setBlackListB51(int  blackListB51) {
        this.blackListB51 = blackListB51;
    }

    public int  getGdTypeZzrCount() {
        return gdTypeZzrCount;
    }

    public void setGdTypeZzrCount(int  gdTypeZzrCount) {
        this.gdTypeZzrCount = gdTypeZzrCount;
    }

    public int  getGdTypeTzzCount() {
        return gdTypeTzzCount;
    }

    public void setGdTypeTzzCount(int  gdTypeTzzCount) {
        this.gdTypeTzzCount = gdTypeTzzCount;
    }

    public int  getGdTypeQyCount() {
        return gdTypeQyCount;
    }

    public void setGdTypeQyCount(int  gdTypeQyCount) {
        this.gdTypeQyCount = gdTypeQyCount;
    }

    public int  getGdTypeQtCount() {
        return gdTypeQtCount;
    }

    public void setGdTypeQtCount(int  gdTypeQtCount) {
        this.gdTypeQtCount = gdTypeQtCount;
    }

    public int  getGdTypeFrCount() {
        return gdTypeFrCount;
    }

    public void setGdTypeFrCount(int  gdTypeFrCount) {
        this.gdTypeFrCount = gdTypeFrCount;
    }

    public int  getCorpCxCount() {
        return corpCxCount;
    }

    public void setCorpCxCount(int  corpCxCount) {
        this.corpCxCount = corpCxCount;
    }

    public int  getCorpTyCount() {
        return corpTyCount;
    }

    public void setCorpTyCount(int  corpTyCount) {
        this.corpTyCount = corpTyCount;
    }

    public int  getCorpDxCount() {
        return corpDxCount;
    }

    public void setCorpDxCount(int  corpDxCount) {
        this.corpDxCount = corpDxCount;
    }

    public int  getCorpZxCount() {
        return corpZxCount;
    }

    public void setCorpZxCount(int  corpZxCount) {
        this.corpZxCount = corpZxCount;
    }

    public int  getCorpQcCount() {
        return corpQcCount;
    }

    public void setCorpQcCount(int  corpQcCount) {
        this.corpQcCount = corpQcCount;
    }

    public int  getCorpQtCount() {
        return corpQtCount;
    }

    public void setCorpQtCount(int  corpQtCount) {
        this.corpQtCount = corpQtCount;
    }

    public int  getIndividualCxCount() {
        return individualCxCount;
    }

    public void setIndividualCxCount(int  individualCxCount) {
        this.individualCxCount = individualCxCount;
    }

    public int  getIndividualTyCount() {
        return individualTyCount;
    }

    public void setIndividualTyCount(int  individualTyCount) {
        this.individualTyCount = individualTyCount;
    }

    public int  getIndividualDxCount() {
        return individualDxCount;
    }

    public void setIndividualDxCount(int  individualDxCount) {
        this.individualDxCount = individualDxCount;
    }

    public int  getIndividualZxCount() {
        return individualZxCount;
    }

    public void setIndividualZxCount(int  individualZxCount) {
        this.individualZxCount = individualZxCount;
    }

    public int  getIndividualQcCount() {
        return individualQcCount;
    }

    public void setIndividualQcCount(int  individualQcCount) {
        this.individualQcCount = individualQcCount;
    }

    public int  getIndividualQtCount() {
        return individualQtCount;
    }

    public void setIndividualQtCount(int  individualQtCount) {
        this.individualQtCount = individualQtCount;
    }

    public int  getExecutiveinfoCount() {
        return executiveinfoCount;
    }

    public void setExecutiveinfoCount(int  executiveinfoCount) {
        this.executiveinfoCount = executiveinfoCount;
    }

    public String getProvinceCode() {
        return provinceCode;
    }

    public void setProvinceCode(String provinceCode) {
        this.provinceCode = provinceCode;
    }

    public String getCityCode() {
        return cityCode;
    }

    public void setCityCode(String cityCode) {
        this.cityCode = cityCode;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("");
        sb.append(idhashsha256).append(",");
        sb.append(provinceLine).append(",");
        sb.append(province).append(",");
        sb.append(citylevel).append(",");
        sb.append(avgAmountPerMonthLevel).append(",");
        sb.append(judicativeType).append(",");
        sb.append(isnotwork).append(",");
        sb.append(gender).append(",");
        sb.append(networkLoan).append(",");
        sb.append(hascarOrnot).append(",");
        sb.append(collectionLoan).append(",");
        sb.append(losecreditTag).append(",");
        sb.append(age).append(",");
        sb.append(amountPerMonth).append(",");
        sb.append(salary).append(",");
        sb.append(flightcount).append(",");
        sb.append(fundLastCompanyMonthNum).append(",");
        sb.append(companyNumLast24month).append(",");
        sb.append(idnumNameCount).append(",");
        sb.append(idnumMobileCount).append(",");
        sb.append(idnumEmailCount).append(",");
        sb.append(idnumQqCount).append(",");
        sb.append(imeiCount).append(",");
        sb.append(flightTime).append(",");
        sb.append(trainTakerCount).append(",");
        sb.append(trainBuyerCount).append(",");
        sb.append(investInfo).append(",");
        sb.append(eBusinessCount).append(",");
        sb.append(blackListA1).append(",");
        sb.append(blackListA2).append(",");
        sb.append(blackListA3).append(",");
        sb.append(blackListA4).append(",");
        sb.append(blackListA5).append(",");
        sb.append(blackListA6).append(",");
        sb.append(blackListA7).append(",");
        sb.append(blackListA8).append(",");
        sb.append(blackListA9).append(",");
        sb.append(blackListA10).append(",");
        sb.append(blackListA11).append(",");
        sb.append(blackListB20).append(",");
        sb.append(blackListB50).append(",");
        sb.append(blackListB51).append(",");
        sb.append(gdTypeZzrCount).append(",");
        sb.append(gdTypeTzzCount).append(",");
        sb.append(gdTypeQyCount).append(",");
        sb.append(gdTypeQtCount).append(",");
        sb.append(gdTypeFrCount).append(",");
        sb.append(corpCxCount).append(",");
        sb.append(corpTyCount).append(",");
        sb.append(corpDxCount).append(",");
        sb.append(corpZxCount).append(",");
        sb.append(corpQcCount).append(",");
        sb.append(corpQtCount).append(",");
        sb.append(individualCxCount).append(",");
        sb.append(individualTyCount).append(",");
        sb.append(individualDxCount).append(",");
        sb.append(individualZxCount).append(",");
        sb.append(individualQcCount).append(",");
        sb.append(individualQtCount).append(",");
        sb.append(executiveinfoCount).append(",");
        sb.append(provinceCode).append(",");
        sb.append(cityCode);
        return sb.toString();
    }
}
