package com.beyand.pgsql.service;

import com.beyand.openid.OpenIdConvertUtils;

import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;

public class Test {
    public static void main(String[] args) {
        String id ="330381199407296432";
        String openid = OpenIdConvertUtils.getOpenId(sha256(id).toLowerCase(),"0").getString("OpenId");
        System.out.println(openid);
    }

    static String sha256(String text) {
        byte[] bts;
        try {
            bts = text.getBytes("UTF-8");
            MessageDigest md = MessageDigest.getInstance("SHA-256");
            byte[] bts_hash = md.digest(bts);
            StringBuffer buf = new StringBuffer();
            for (byte b : bts_hash) {
                buf.append(String.format("%02X", b & 0xff));
            }
            return buf.toString();
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
            return "";
        } catch (java.security.NoSuchAlgorithmException e) {
            e.printStackTrace();
            return "";
        }
    }

}
