package com.beyand.pgsql.service;
import com.beyand.openid.OpenIdConvertUtils;
import com.csvreader.CsvWriter;

import java.io.*;
import java.nio.charset.Charset;
import java.security.MessageDigest;
import java.util.ArrayList;


public class TestCSV {
    public static  ArrayList<String> inputArray=null;
    // 读取csv文件的内容
    public static ArrayList<String> readCsv(String filepath) {
        File csv = new File(filepath); // CSV文件路径
        csv.setReadable(true);//设置可读
        csv.setWritable(true);//设置可写
        BufferedReader br = null;
        try {
            br = new BufferedReader(new FileReader(csv));
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        String line = "";
        String id = "";
        String name = "";
        String result ="";
        ArrayList<String> allString = new ArrayList<String>();
        String openid ="";
        String idsha256="";
        try {
            while ((line = br.readLine()) != null) // 读取到的内容给line变量
            {
                id = line.split(",")[0];
                name = line.split(",")[1];
                idsha256= sha256(id).toLowerCase();
                System.out.println(idsha256);
                openid = OpenIdConvertUtils.getOpenId(idsha256,"0").getString("OpenId");
                result = QueryTianWang.queryTianWang(openid);
//                result ="test";
                allString.add(name+",'"+id+","+idsha256+","+result);
            }
            System.out.println("csv表格中所有行数：" + allString.size());
        } catch (Exception e1) {
            e1.printStackTrace();
            writeCSV("D:\\testOutput.csv",inputArray);
            System.out.println(name+"|"+"id");
        }

        return allString;

    }

    public static void writeCSV(String path, ArrayList<String> writearraylist ) {
        String csvFilePath = path;

        try {

            // 创建CSV写对象 例如:CsvWriter(文件路径，分隔符，编码格式);
            CsvWriter csvWriter = new CsvWriter(csvFilePath, ',', Charset.forName("GBK"));
            // 写内容
            String[] headers = {"姓名","身份證號","idsha256","openid","data"};
            csvWriter.writeRecord(headers);
            for(int i=0;i < writearraylist.size();i++){
                String[] writeLine=writearraylist.get(i).split(",");
                csvWriter.writeRecord(writeLine);
            }

            csvWriter.close();
            System.out.println("--------CSV文件已经写入--------");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    static String sha256(String text) {
        byte[] bts;
        try {
            bts = text.getBytes("UTF-8");
            MessageDigest md = MessageDigest.getInstance("SHA-256");
            byte[] bts_hash = md.digest(bts);
            StringBuffer buf = new StringBuffer();
            for (byte b : bts_hash) {
                buf.append(String.format("%02X", b & 0xff));
            }
            return buf.toString();
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
            return "";
        } catch (java.security.NoSuchAlgorithmException e) {
            e.printStackTrace();
            return "";
        }
    }
    public static void main(String[] args) {
        long startTime = System.currentTimeMillis();
        inputArray = readCsv("D:\\Testdata\\impgateway\\input.csv");
        writeCSV("D:\\testOutput.csv",inputArray);
        long endTime = System.currentTimeMillis();
        System.out.println("程序运行时间：" + (endTime - startTime)/1000 + "s");
    }
}
