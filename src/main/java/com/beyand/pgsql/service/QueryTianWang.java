package com.beyand.pgsql.service;

import com.beyand.pgsql.domain.SkynetSaturabilityRaw;
import org.apache.ibatis.io.Resources;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.ibatis.session.SqlSessionFactoryBuilder;

import java.io.IOException;
import java.io.InputStream;

public class QueryTianWang {
    private static SqlSessionFactory sessionFactory;
    private static String result;
    public static String queryTianWang(String openid) {
        //通过SqlSessionFactory打开一个会话
        SqlSession openSession = null;
        SkynetSaturabilityRaw skynetSaturabilityRaw=null;
        try {
            openSession = getSession();
            skynetSaturabilityRaw = openSession.selectOne("getSkeyNetBeanById",openid);
            if(null == skynetSaturabilityRaw){
                result = "";
            }else{
                result=skynetSaturabilityRaw.toString();
            }
//            System.out.println(skynetSaturabilityRaw.toString());

        } catch (IOException e) {
            e.printStackTrace();
        }finally {
            //关闭session会话
            openSession.close();
        }

        return  result;
    }

    public static SqlSession getSession() throws IOException {
        // 加载配置文件
        InputStream is = Resources.getResourceAsStream("mybatis-config.xml");
        // 获取session
        SqlSessionFactory ssf = new SqlSessionFactoryBuilder().build(is);
        SqlSession sqlSession = ssf.openSession();
        return sqlSession;
    }
}
