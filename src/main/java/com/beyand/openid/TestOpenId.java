package com.beyand.openid;

import com.alibaba.fastjson.JSONObject;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;

public class TestOpenId {
    public static void main(String[] args) {
        String paramin = "IdNumber=" + "B78071E9C11D77FC0FD8B0E97CB935CA";

        JSONObject paramot = params2Map(paramin);
        String idNumber = paramot.getString("IdNumber");
        System.out.println("转换前="+idNumber);

        String openId= OpenIdConvertUtils.getOpenId(idNumber.toLowerCase(),"1").getString("OpenId");
        System.out.println("转换后="+openId);
    }


    public static JSONObject params2Map(String param){
        String np = null;
        try {
            np = URLDecoder.decode(param,"UTF-8");
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
//            参数无法解析
            System.out.println("参数无法解析");
        }
        JSONObject object = new JSONObject();
        String[] ps = np.split("&");
        for (String p:ps){
            String[] kv = p.split("=");
            object.put(kv[0],kv[1]);
        }
        return object;
    }
}

